// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:loggin_google/core/models/api/article.dart';
import 'package:loggin_google/views/screens/toon_tube/community/components/post_list.dart';

class FavoriteScreen extends StatefulWidget {
  List<Map<String, Object?>> favorites;

  FavoriteScreen({Key? key, required this.favorites}) : super(key: key);

  @override
  _FavoriteScreenState createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  @override
  Widget build(BuildContext context) {
    List<Article> articles =
        widget.favorites.map((dynamic item) => Article.fromJson(item)).toList();
    return Scaffold(
        appBar: AppBar(title: const Text("Favorites")),
        body: ListView.builder(
          itemCount: articles.length,
          itemBuilder: (context, index) => PostList(post: articles[index]),
        ));
  }
}
