import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/views/screens/toon_tube/community/community_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/home/home_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/profile/profile_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/watch/watch_screen.dart';

class MainHome extends StatefulWidget {
  const MainHome({Key? key}) : super(key: key);

  @override
  _MainHomeState createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  final GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();
  DateTime timeBackPress = DateTime.now();
  late FlutterLocalNotificationsPlugin localNotificationsPlugin;
  late FirebaseMessaging messaging;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var androidInitialize = const AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOSInitialize = const IOSInitializationSettings();
    var initializeSettings = InitializationSettings(
      android: androidInitialize,
      iOS: iOSInitialize,
    );

    localNotificationsPlugin = FlutterLocalNotificationsPlugin();
    localNotificationsPlugin.initialize(initializeSettings);

    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value){
        print(value);
    });
        FirebaseMessaging.onMessage.listen((RemoteMessage event) {
        print("message recieved");
        print(event.notification!.body);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('Message clicked!');
    });

  }

  var allPages = [
    const Home(),
    const Community(),
    const Watch(),
    const Profile()
  ];
  int Index = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final difference = DateTime.now().difference(timeBackPress);
        final ifExitWarning = difference >= Duration(seconds: 2);
        timeBackPress = DateTime.now();
        if (ifExitWarning) {
          const message = 'press back again to exit app';
          Fluttertoast.showToast(
              msg: message, fontSize: 18, textColor: Colors.red);
          return false;
        } else {
          Fluttertoast.cancel();
          SystemNavigator.pop();
          showNotification();
          return true;
        }
      },
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          body: allPages[Index],
          bottomNavigationBar: CurvedNavigationBar(
            key: _bottomNavigationKey,
            buttonBackgroundColor: Constant.primaryColor,
            backgroundColor: Colors.transparent,
            items: <Widget>[
              Icon(
                (Index == 0) ? Icons.home_outlined : Icons.home,
                color: Colors.white,
              ),
              Icon(
                (Index == 1) ? Icons.article_outlined : Icons.article,
                color: Colors.white,
              ),
              Icon(
                (Index == 2) ? Icons.live_tv_outlined : Icons.live_tv,
                color: Colors.white,
              ),
              Icon(
                (Index == 3) ? Icons.person_outline : Icons.person,
                color: Colors.white,
              )
            ],
            height: 60,
            onTap: (index) {
              setState(() {
                Index = index;
              });
            },
            letIndexChange: (index) => true,
            animationDuration: const Duration(milliseconds: 700),
            animationCurve: Curves.easeInOut,
            color: Constant.primaryColor,
          ),
        ),
      ),
    );
  }

  Future showNotification() async {
    var androidDetails = const AndroidNotificationDetails(
        "channelId", "Local Notification",
        channelDescription: "hi from my app and thanks for use",
      importance: Importance.high,

    );
    var iosDetails=const IOSNotificationDetails();
    var generalDetails=NotificationDetails(android: androidDetails,iOS: iosDetails);
    await localNotificationsPlugin.show(0, "ToonTube Notification ", "you close app", generalDetails);
  }
}
