import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/views/screens/toon_tube/community/components/category_new_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/community/components/post_text.dart';
import 'package:loggin_google/views/screens/toon_tube/community/components/search.dart';
import 'package:loggin_google/views/shared/app_drawer.dart';
import 'package:loggin_google/views/shared/menu_button.dart';

class Community extends StatefulWidget {
  const Community({Key? key}) : super(key: key);

  @override
  _CommunityState createState() => _CommunityState();
}

class _CommunityState extends State<Community> {
  @override
  
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: SafeArea(
        child: DefaultTabController(
          length: 6,
          child: Scaffold(
            backgroundColor: Constant.secondaryColor,
            appBar: AppBar(
              title: Constant.titleAppBar,
              backgroundColor: Constant.primaryColor,
              elevation: 6,
              centerTitle: true,
              actions: <Widget>[
                IconButton(
                    onPressed: () {
                      showSearch(context: context, delegate: DataSearch());
                    },
                    icon: const Icon(Icons.search)),
                const MenuButton(),
              ],
              bottom: TabBar(
                isScrollable: true,
                indicatorColor: Constant.secondaryColor,
                indicatorWeight: 2,
                labelColor: Constant.secondaryColor,
                labelStyle:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                unselectedLabelStyle: const TextStyle(),
                tabs: const <Widget>[
                  Tab(
                    text: "جديد",
                  ),
                  Tab(
                    text: "شائع",
                  ),
                  Tab(
                    text: "القسم الأول",
                  ),
                  Tab(
                    text: "القسم الثاني",
                  ),
                  Tab(
                    text: "القسم الثالث",
                  ),
                  Tab(
                    text: "القسم الرابع",
                  ),
                ],
              ),
            ),
            drawer:  AppDrawer(),
            body: TabBarView(children: [
              const CategoryNew(),
              ListView(
                children: const [
                  PostText(),
                  PostText(),
                  PostText(),
                ],
              ),
              ListView(
                children: const [],
              ),
              const SizedBox(
                height: 200,
                child: Center(
                  child: Text(
                    "Tab 4",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              const SizedBox(
                height: 200,
                child: Center(
                  child: Text(
                    "Tab 5",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              const SizedBox(
                height: 200,
                child: Center(
                  child: Text(
                    "Tab 6",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ]),
          ),
        ),
      ),
    );
  }
}
