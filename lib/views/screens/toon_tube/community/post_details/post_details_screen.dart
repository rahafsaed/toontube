import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/api/article.dart';
import 'package:loggin_google/core/models/database/db_helper.dart';
import 'package:loggin_google/views/screens/toon_tube/community/post_details/components/comment_box.dart';
import 'package:loggin_google/views/screens/toon_tube/community/post_details/components/rating.dart';
import 'package:loggin_google/views/shared/shared_functions.dart';
import 'package:sliding_sheet/sliding_sheet.dart';

class PostDetail extends StatefulWidget {
  final Article post;
  final bool isFavorite;

  const PostDetail({Key? key, required this.post, required this.isFavorite})
      : super(key: key);

  @override
  State<PostDetail> createState() => _PostDetailState();
}

class _PostDetailState extends State<PostDetail> {
  double custFontSize = 20;

  bool visibleTitle = true;
  Color postInfoIconColor = Colors.blue;
  String category = 'Animation';

  var top = 0.0;

  String? title, author, postDate, imgURL, details;
  double? rate;
  bool? isFavorite;

  @override
  void initState() {
    title = widget.post.title;
    author = "Hello World";
    postDate = "2021-10-12";
    imgURL = widget.post.urlToImage;
    rate = 4;
    details = widget.post.content;

    isFavorite = widget.isFavorite;
    super.initState();
  }

  //Change Font Size..................................
  void changeFontSizeBigger() async {
    setState(() {
      custFontSize += 2;
    });
  }

  void changeFontSizeSmaller() async {
    setState(() {
      custFontSize -= 2;
      if (custFontSize < 6) {
        custFontSize = 6;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SliverAppBar detailAppBar = SliverAppBar(
      flexibleSpace: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        top = constraints.biggest.height;
        return FlexibleSpaceBar(
          centerTitle: true,
          title: Visibility(
            child: Padding(
              padding: const EdgeInsets.only(left: 40, right: 40),
              child: Text(
                title!,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
            visible: top == 56.0 ? true : false,
          ),
          background: Image.network(
            imgURL!,
            fit: BoxFit.cover,
          ),
        );
      }),
      expandedHeight: 300.0,
      floating: true,
      pinned: true,
      backgroundColor: Colors.teal[400],
      leading: IconButton(
        icon: const Icon(Icons.arrow_back),
        tooltip: 'Back',
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );

    Widget postHeaderInformation = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title!,
          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
        ),
        Row(
          children: [
            getRichText(Icons.edit, author!),
            getRichText(Icons.date_range, postDate!),
            addHorizontalSpace(MediaQuery.of(context).size.width * 0.3),
            IconButton(
              icon: Icon(
                  isFavorite! ? Icons.favorite_sharp : Icons.favorite_border),
              color: Colors.red,
              onPressed: () {
                setState(() {
                  if (!isFavorite!) {
                    DBHelper.insert(widget.post);
                    isFavorite = true;
                  } else {
                    DBHelper.delete(widget.post);
                    isFavorite = false;
                  }
                });
              },
            ),
          ],
        ),
        const Rating(),
        buildCategoryChips(category),
      ],
    );

    Widget postBodyInformation = SingleChildScrollView(
      child: Column(
        children: [
          addVerticalSpace(10),
          Text(
            details!,
            style: TextStyle(
              fontSize: custFontSize,
              fontWeight: FontWeight.w400,
              height: 2.0,
            ),
          ),
          //addVerticalSpace(400)
        ],
      ),
    );

    return SafeArea(
      child: Scaffold(
          body: CustomScrollView(
        slivers: <Widget>[
          detailAppBar,
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  child: postHeaderInformation,
                  padding: const EdgeInsets.all(8.0),
                ),
                const Divider(),
                Padding(
                  padding: const EdgeInsets.only(right: 16.0, left: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        "Details",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                            icon: const Icon(Icons.comment),
                            tooltip: 'Comment Icon',
                            color: Colors.teal,
                            onPressed: showAsBottomSheet,
                          ),

                          // ignore: deprecated_member_use
                          FlatButton(
                              splashColor: Constant.secondaryColor,
                              minWidth: 5,
                              height: 5,
                              onPressed: changeFontSizeSmaller,
                              child: Image.asset('assets/images/smal.jpg')),

                          // ignore: deprecated_member_use
                          FlatButton(
                              splashColor: Constant.secondaryColor,
                              minWidth: 10,
                              height: 10,
                              onPressed: changeFontSizeBigger,
                              child: Image.asset('assets/images/add.jpg'))
                        ],
                      ),
                    ],
                  ),
                ),
                buildContainer(postBodyInformation, context),
              ],
            ),
          )
        ],
      )),
    );
  }

  void showAsBottomSheet() async {
    await showSlidingBottomSheet(context, builder: (context) {
      return SlidingSheetDialog(
        elevation: 8,
        cornerRadius: 16,
        snapSpec: const SnapSpec(
          snap: true,
          snappings: [0.4, 0.7, 1.0],
          positioning: SnapPositioning.relativeToAvailableSpace,
        ),
        builder: (context, state) {
          return Container(
            color: Constant.secondaryColor,
            height: 451,
            child: Center(
              child: Material(child: buildSheet()),
            ),
          );
        },
      );
    });
  }

  Widget makeDimissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          Navigator.pop(context);
        },
        child: GestureDetector(
          child: child,
          onTap: () {},
        ),
      );

  Widget buildSheet() => makeDimissible(
      child: DraggableScrollableSheet(
          initialChildSize: 0.9,
          minChildSize: 0.5,
          maxChildSize: 1,
          builder: (_, ScrollController scrollController) => Container(
              //color: Colors.redAccent,
              decoration: BoxDecoration(
                  color: Constant.secondaryColor,
                  borderRadius:
                      const BorderRadius.vertical(top: Radius.circular(20))),
              child: Padding(
                  padding: MediaQuery.of(context).viewInsets,
                  child: const CommentBoxx()))));
}
