import 'package:comment_box/comment/comment.dart';
import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/session.dart';
import 'package:loggin_google/core/models/user_info.dart';

class CommentBoxx extends StatefulWidget {
  const CommentBoxx({Key? key}) : super(key: key);

  @override
  _CommentBoxxState createState() => _CommentBoxxState();
}

class _CommentBoxxState extends State<CommentBoxx> {


  final formKey = GlobalKey<FormState>();
  final TextEditingController commentController = TextEditingController();

  late Session session ;
  late Future<UserInfo> userInfo;
  @override
  void initState() {
    super.initState();
    session = Session("","","");
     userInfo = session.loadUserInfo();
  }

  List filedata = [
    {
      'name': 'Adeleye Ayodeji',
      'pic': 'https://picsum.photos/300/30',
      'message': 'I love to code'
    },
    {
      'name': 'Biggi Man',
      'pic': 'https://picsum.photos/300/30',
      'message': 'Very cool'
    },
    {
      'name': 'Biggi Man',
      'pic': 'https://picsum.photos/300/30',
      'message': 'Very cool'
    },
    {
      'name': 'Biggi Man',
      'pic': 'https://picsum.photos/300/30',
      'message': 'Very cool'
    },
  ];

  Widget commentChild(data) {
    return ListView(
      children: [
        for (var i = 0; i < data.length; i++)
          Padding(
            padding: const EdgeInsets.fromLTRB(2.0, 8.0, 2.0, 0.0),
            child: ListTile(
              
              leading: GestureDetector(
                onTap: () async {
                },
                child: Container(
                  height: 50.0,
                  width: 50.0,
                  decoration: const BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(50))),
                  child: CircleAvatar(
                      radius: 50,
                      backgroundImage: NetworkImage(data[i]['pic'] + "$i")),
                ),
              ),
              title: Text(
                data[i]['name'],
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(data[i]['message']),
            ),
          )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {

    return Center(
      child: FutureBuilder<UserInfo>(
        future: userInfo,
        builder: ( context,  snapshot) {
          if (snapshot.hasData) {
            //Comment_Box.myPhoto = myMap["photoUrl"];
            return CommentBox(
              userImage:snapshot.data!.photoUrl,
              child: commentChild(filedata),
              labelText: 'Write a comment...${snapshot.data!.displayName}',
              withBorder: false,
              errorText: 'Comment cannot be blank',
              sendButtonMethod: () {
                if (formKey.currentState!.validate()) {
                  //print(commentController.text);
                  setState(() {
                    var value = {
                      'name': snapshot.data!.displayName,
                      'pic':snapshot.data!.photoUrl,
                      'message': commentController.text
                    };
                    filedata.insert(0, value);
                  });
                  commentController.clear();
                  FocusScope.of(context).unfocus();
                } else {
                  //print("Not validated");
                }
              },
              formKey: formKey,
              commentController: commentController,
              backgroundColor: Constant.secondaryColor,
              textColor: Colors.black,
              sendWidget:
                  const Icon(Icons.send_sharp, size: 30, color: Colors.teal),
            );
          } else {
            return const Text(" ");
          }
        },
      ),
    );
  }
}
