import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class Rating extends StatefulWidget {
  const Rating({Key? key}) : super(key: key);

  @override
  _RatingState createState() => _RatingState();
}

class _RatingState extends State<Rating> {
  double rating = 0;
  Color ratingColor = Colors.teal;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        TextButton(
          onPressed: () {
            showRating();
          },
          child: Text(
            'Grade Now',
            style: TextStyle(
              fontSize: 20,
              color: ratingColor,
            ),
          ),
        ),
        const Spacer(),
        const Text(
          'Ratings : ',
          style: TextStyle(
            color: Colors.grey,
            fontSize: 16,
          ),
        ),
        Text(
          "$rating",
          style: TextStyle(
            color: ratingColor,
            fontSize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
        const Spacer(flex:1,),
      ],
    );
  }

  void showRating() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: const Text('Rate this post'),
              content: buildRating(),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child:  Text(
                    "OK",
                    style: TextStyle(fontSize: 20, color: ratingColor),
                  ),
                )
              ],
            ));
  }

  Widget buildRating() => RatingBar.builder(
        initialRating: rating,
        minRating: 1,
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: 5,
        itemSize: 20,
        itemPadding: const EdgeInsets.symmetric(vertical: 1.0),
        itemBuilder: (context, _) =>  Icon(
          Icons.star,
          color: ratingColor,
          size: 12,
        ),
        updateOnDrag: true,
        onRatingUpdate: (rating) => setState(() {
          this.rating = rating;
        }),
      );
}
