import 'package:flutter/material.dart';

class PostText extends StatefulWidget {
  const PostText({Key? key}) : super(key: key);

  @override
  _PostTextState createState() => _PostTextState();
}

class _PostTextState extends State<PostText> {
  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 200,
      width: 200,
      child: Card(
        child: ListTile(
          title: Text("Codesinsider.com"),
        ),
        elevation: 8,
        shadowColor: Colors.teal,
        margin: EdgeInsets.all(20),
      ),
    );
  }
}
