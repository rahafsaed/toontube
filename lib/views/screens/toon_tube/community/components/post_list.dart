import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/api/article.dart';
import 'package:loggin_google/core/models/database/db_helper.dart';
import 'package:loggin_google/views/screens/toon_tube/community/post_details/post_details_screen.dart';
import 'package:loggin_google/views/shared/shared_functions.dart';

class PostList extends StatefulWidget {
  final Article post;

  const PostList({Key? key, required this.post}) : super(key: key);

  @override
  _PostListState createState() => _PostListState();
}

class _PostListState extends State<PostList> {
  String? authorName, postDate, imgURL, desc, title;
  double? rate;

  @override
  void initState() {
    authorName = "Technologe";
    postDate = "2021-10-10";
    imgURL = widget.post.urlToImage;
    desc = widget.post.author;
    rate = 4;
    title = widget.post.title;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: Constant.primaryColor,
      elevation: 15.0,
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              FadeInImage.assetNetwork(
                placeholder: "assets/images/loading.gif",
                image: imgURL!,
              ),
              Ink.image(
                image: NetworkImage(
                  imgURL!,
                ),
                child: InkWell(
                  onTap: () async {
                    bool isFavorite =
                        await DBHelper.checkFavorite(widget.post.title);
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => PostDetail(
                          post: widget.post,
                          isFavorite: isFavorite,
                        ),
                      ),
                    );
                  },
                ),
                height: 180,
                fit: BoxFit.cover,
              ),
              Positioned(
                bottom: 10,
                left: 16,
                right: 16,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Hello world',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Constant.secondaryColor,
                        fontSize: 24,
                      ),
                    ),
                    buildRating(),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(10).copyWith(bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    strutStyle: const StrutStyle(fontSize: 12.0),
                    text: TextSpan(
                        style: const TextStyle(color: Colors.black),
                        text: title),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    getRichText(Icons.edit, authorName!),
                    getRichText(Icons.date_range, postDate!),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildRating() => RatingBar.builder(
        initialRating: rate!,
        minRating: 1,
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: 5,
        itemSize: 20,
        itemPadding: const EdgeInsets.symmetric(vertical: 1.0),
        itemBuilder: (context, _) => const Icon(
          Icons.star,
          color: Colors.yellow,
          size: 12,
        ),
        ignoreGestures: true,
        onRatingUpdate: (double value) {},
      );
}
