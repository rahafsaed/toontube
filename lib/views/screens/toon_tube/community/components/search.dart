import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';

class DataSearch extends SearchDelegate<String> {
  List names = ["gvbn", "dvb", "asdfg", "fdv", "dsfv", "fdbv"];
  List recentNames = ["gvbn", "dvb"];

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          query = "";
        },
        icon: Icon(
          Icons.clear,
          color: Constant.primaryColor,
        ),
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.arrow_back,
        color: Constant.primaryColor,
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Text(query);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List suggestionList = query.isEmpty
        ? recentNames
        : names.where((element) => element.contains(query)).toList();

    return ListView.builder(
        itemCount: suggestionList.length,
        itemBuilder: (context, index) {
          return ListTile(
            onTap: () {
              showResults(context);
            },
            title: RichText(
              text: TextSpan(
                  text: suggestionList[index].substring(0, query.length),
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                  children: [
                    TextSpan(
                      text: suggestionList[index].substring(query.length),
                      style: const TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ]),
            ),
          );
        });
  }
}
