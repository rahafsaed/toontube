import 'package:flutter/material.dart';
import 'package:loggin_google/core/models/api/article.dart';
import 'package:loggin_google/core/services/api_service.dart';
import 'package:loggin_google/views/screens/toon_tube/community/components/post_list.dart';
import 'package:lottie/lottie.dart';

class CategoryNew extends StatefulWidget {
  const CategoryNew({Key? key}) : super(key: key);

  @override
  _CategoryNewState createState() => _CategoryNewState();
}

class _CategoryNewState extends State<CategoryNew> {
  final ApiService _api = ApiService();

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        FutureBuilder(
          future: _api.getArticle(),
          builder:
              (BuildContext context, AsyncSnapshot<List<Article>> snapshot) {
            if (snapshot.hasData) {
              List<Article>? articles = snapshot.data;
              return ListView.builder(
                itemCount: articles!.length,
                itemBuilder: (context, index) =>
                    PostList(post: articles[index]),
              );
            }

            return Center(
              child: Lottie.asset("assets/lottie/data.json"),
            );
          },
        );
      },
      child: FutureBuilder(
        future: _api.getArticle(),
        builder: (BuildContext context, AsyncSnapshot<List<Article>> snapshot) {
          if (snapshot.hasData) {
            List<Article>? articles = snapshot.data;
            return ListView.builder(
              itemCount: articles!.length,
              itemBuilder: (context, index) => PostList(post: articles[index]),
            );
          }

          return Center(
            child: Lottie.asset("assets/lottie/data.json"),
          );
        },
      ),
    );
  }
}
