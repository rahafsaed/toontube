import 'package:flutter/material.dart';
import 'package:loggin_google/views/screens/toon_tube/community/components/category_new_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/watch/watch_screen.dart';

class MakeSection extends StatefulWidget {
  const MakeSection({Key? key}) : super(key: key);

  @override
  _MakeSectionState createState() => _MakeSectionState();
}

class _MakeSectionState extends State<MakeSection> {
  double custFontSize = 12;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Wrap(

        direction: Axis.horizontal,
        spacing: 10,
        children: [
          makeSection(
              "New Category", "assets/images/icon.PNG", const CategoryNew()),
          makeSection("Popular", "assets/images/icon.PNG", const CategoryNew()),
          makeSection("Video", "assets/images/icon.PNG", const Watch()),
        ],
      ),
    );
  }

  Widget makeSection(String? name, String? imageUrl, Widget dis) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => dis));
      },
      child: Column(
        children: [
          Image.asset(
            imageUrl!,
            height: 100,
          ),
          Text(
            name!,
            style: TextStyle(
              fontSize: custFontSize,
              fontWeight: FontWeight.bold,
              height: 1.5,
            ),
          ),
        ],
      ),
    );
  }

  Widget makeSection1(String? name, String? imageUrl) {
    return Column(
      children: [
        Ink.image(
          image: AssetImage(
            imageUrl!,
          ),
          child: InkWell(
            onTap: () {},
          ),
          height: 100,
        ),
        Text(
          name!,
          style: TextStyle(
            fontSize: custFontSize,
            fontWeight: FontWeight.bold,
            height: 1.5,
          ),
        ),
      ],
    );
  }
}
