import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/views/shared/shared_functions.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class MakeCarousel extends StatefulWidget {
  const MakeCarousel({Key? key}) : super(key: key);

  @override
  _MakeCarouselState createState() => _MakeCarouselState();
}

class _MakeCarouselState extends State<MakeCarousel> {
  int activeIndex = 0;
  final List<String> imageList = [
    "assets/images/icon.PNG",
    "assets/images/spacetoon.png",
    "assets/images/videoplaye.png",
    "assets/images/profile.png",
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: Column(
        children: [
          addVerticalSpace(20),
          createCarousel(),
          addVerticalSpace(20),
          buildIndicator(),
        ],
      ),
    );
  }

  Widget createCarousel() {
    return CarouselSlider(
      options: CarouselOptions(
        enlargeCenterPage: true,
        enableInfiniteScroll: false,
        autoPlay: true,
        pageSnapping: false,
        height: 150,
        onPageChanged: (index, reason) => setState(() => activeIndex = index),
      ),
      items: imageList
          .map((e) => ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    Image.asset(
                      e,
                      fit: BoxFit.scaleDown,
                    )
                  ],
                ),
              ))
          .toList(),
    );
  }

  Widget buildIndicator() => AnimatedSmoothIndicator(
        activeIndex: activeIndex,
        count: imageList.length,
        effect: ScaleEffect(
          dotColor: Constant.secondaryColor,
          dotWidth: 10,
          dotHeight: 10,
          activeDotColor: Constant.primaryColor,
        ),
      );
}
