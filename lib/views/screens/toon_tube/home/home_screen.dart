import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/views/screens/toon_tube/home/components/make_carousel.dart';
import 'package:loggin_google/views/screens/toon_tube/home/components/make_section.dart';
import 'package:loggin_google/views/shared/app_drawer.dart';
import 'package:loggin_google/views/shared/menu_button.dart';
import 'package:loggin_google/views/shared/shared_functions.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Constant.secondaryColor,
        appBar: AppBar(
          title: Constant.titleAppBar,
          backgroundColor: Constant.primaryColor,
          elevation: 6,
          centerTitle: true,
          actions: const <Widget>[
            MenuButton(),
          ],
        ),
        drawer:  AppDrawer(),
        body: Column(
          children: [
            const MakeCarousel(),
            Divider(
              color: Constant.primaryColor,
              thickness: 5,
            ),
            addVerticalSpace(20),
            const MakeSection()
          ],
        ),
      ),
    );
  }
}
