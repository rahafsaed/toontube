import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loggin_google/core/models/api/model_video.dart';

import '../../../../../better_player.dart';


class ContentTheList extends StatefulWidget {
  const ContentTheList({Key? key}) : super(key: key);

  @override
  ContentTheListState createState() => ContentTheListState();
}

class ContentTheListState extends State<ContentTheList> {

  ModelVideo modelVideo = ModelVideo();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Card(
          child: Column(
            children: [
              AspectRatio(
                  aspectRatio: 16 / 9,
                  child: BetterPlayerPlaylist(
                      betterPlayerConfiguration: const BetterPlayerConfiguration(
                          controlsConfiguration: BetterPlayerControlsConfiguration(
                            enableAudioTracks: false,
                            enableSubtitles: false,
                            enableFullscreen: true,
                            textColor: Colors.white,
                            iconsColor: Colors.white,
                            backgroundColor: Colors.black38,

                          )),
                      betterPlayerPlaylistConfiguration:
                      const BetterPlayerPlaylistConfiguration(),
                      betterPlayerDataSourceList: createDataSet())),
              const SizedBox(height: 5),
              Text(
                modelVideo.title,
                style: const TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 15),
              ),
              Text(
                modelVideo.desc,
                style: const TextStyle(
                    fontWeight: FontWeight.normal, fontSize: 10),
              ),
              const SizedBox(height: 7),
              Row(
                children: [
                  Text(
                    modelVideo.views,
                    style: const TextStyle(
                      fontSize: 10,
                    ),
                  ),
                  Text(
                    modelVideo.timeVideo,
                    style: const TextStyle(
                      fontSize: 10,
                    ),
                  ),
                ],
              )
            ],
          ),
        ));
  }
}

List<BetterPlayerDataSource> createDataSet() {
  List dataSourceList = <BetterPlayerDataSource>[];
  dataSourceList.add(
    BetterPlayerDataSource(
      BetterPlayerDataSourceType.network,
      "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8",
    ),
  );
  dataSourceList.add(
    BetterPlayerDataSource(BetterPlayerDataSourceType.network,
        "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"),
  );
  dataSourceList.add(
    BetterPlayerDataSource(BetterPlayerDataSourceType.network,
        "http://www.streambox.fr/playlists/test_001/stream.m3u8"),
  );
  return dataSourceList.cast();
}
