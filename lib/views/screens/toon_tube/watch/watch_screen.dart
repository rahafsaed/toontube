import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/views/screens/toon_tube/watch/components/content_the_list.dart';
import 'package:loggin_google/views/shared/app_drawer.dart';
import 'package:loggin_google/views/shared/menu_button.dart';


class Watch extends StatefulWidget {
  const Watch({Key? key}) : super(key: key);

  @override
  _WatchState createState() => _WatchState();
}

class _WatchState extends State<Watch> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Constant.titleAppBar,
          backgroundColor: Constant.primaryColor,
          elevation: 6,
          centerTitle: true,
          actions: const <Widget>[MenuButton()],
        ),
        drawer:  AppDrawer(),
        body: ListView.builder(
          itemCount: 10,
          itemBuilder: (context, index) {
            return const ContentTheList();
          },
        ),
      ),
    );

  }
}
