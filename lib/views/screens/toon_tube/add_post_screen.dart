import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:keyboard_actions/keyboard_actions_config.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/session.dart';
import 'package:loggin_google/core/models/user_info.dart';

class AddPost extends StatefulWidget {
  const AddPost({Key? key}) : super(key: key);

  @override
  _AddPostState createState() => _AddPostState();
}

class _AddPostState extends State<AddPost> {
  Session? session;

  @override
  void initState() {
    super.initState();
    session = Session("", "", "");
  }

  TextEditingController writingTextController = TextEditingController();
  TextEditingController writingBodyController = TextEditingController();
  FocusNode writingTextFocus = FocusNode();
  FocusNode writingBodyFocus = FocusNode();
  File? imageFile;

  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      nextFocus: true,
      actions: [
        KeyboardActionsItem(
          displayArrows: false,
          focusNode: writingBodyFocus,
          toolbarButtons: [
                (node) {
              return GestureDetector(
                onTap: _pickImage,
                child: Container(
                  color: Colors.grey[200],
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: const <Widget>[
                      Icon(Icons.add_photo_alternate, size: 28),
                      Text(
                        "Add Image",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              );
            },
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Future<UserInfo> userInfo = session!.loadUserInfo();
    final size = MediaQuery.of(context).size;

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Constant.primaryColor,
          title: const Text("Write Post"),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  textStyle: const TextStyle(fontSize: 20),
                  primary: Constant.secondaryColor,
                  onPrimary: Constant.primaryColor,
                ),
                onPressed: () {},
                child: Text(
                  "Post",
                  style: TextStyle(color: Constant.primaryColor),
                ),
              ),
            )
          ],
        ),
        body: Stack(
          children: [
            KeyboardActions(
              config: _buildConfig(context),
              child: Column(
                children: [
                  SizedBox(
                    width: size.width,
                    height: size.height -
                        MediaQuery
                            .of(context)
                            .viewInsets
                            .bottom -
                        80,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 14, left: 10),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FutureBuilder<UserInfo>(
                              future: userInfo,
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  return Row(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CircleAvatar(
                                          backgroundImage: NetworkImage(
                                              snapshot.data!.photoUrl!),
                                          radius: 30,
                                        ),
                                      ),
                                      Text(
                                        snapshot.data!.displayName!,
                                        style: const TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  );
                                } else {
                                  return Text("${snapshot.error}");
                                }
                              },
                            ),
                            const Divider(
                              height: 1,
                              color: Colors.black,
                            ),
                            TextFormField(
                              autofocus: true,
                              focusNode: writingTextFocus,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Writing title',
                              ),
                              cursorColor: Constant.primaryColor,
                              cursorHeight: 30,
                              controller: writingTextController,
                              keyboardType: TextInputType.multiline,
                              maxLines: 2,
                            ),
                          TextFormField(
                              autofocus: true,
                              focusNode: writingBodyFocus,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Writing body',
                              ),
                              cursorColor: Constant.primaryColor,
                              cursorHeight: 30,
                              controller: writingBodyController,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                            ),
                            imageFile != null
                                ? Image.file(imageFile!)
                                : Container(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _pickImage() async {
    final pickedImage =
        await ImagePicker().pickImage(source: ImageSource.gallery);
    imageFile = pickedImage != null ? File(pickedImage.path) : null;
  }
}
