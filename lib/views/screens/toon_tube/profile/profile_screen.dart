import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/session.dart';
import 'package:loggin_google/core/models/user_info.dart';
import 'package:loggin_google/views/screens/login_screen.dart';
import 'package:loggin_google/views/shared/shared_functions.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  late Session session;

  late Future<UserInfo> userInfo;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    session = Session("", "", "");
    userInfo = session.loadUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Constant.secondaryColor,
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Constant.secondaryColor,
                    Constant.primaryColor,
                  ],
                  begin: FractionalOffset.bottomCenter,
                  end: FractionalOffset.topCenter,
                ),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  FutureBuilder<UserInfo>(
                    future: userInfo,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Center(
                          child: Column(
                            children: [
                              CircleAvatar(
                                backgroundImage:
                                    NetworkImage(snapshot.data!.photoUrl!),
                                radius: 50,
                              ),
                              addVerticalSpace(10),
                              Text(
                                snapshot.data!.displayName!,
                                style: const TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              addVerticalSpace(10),
                              Text(
                                snapshot.data!.email!,
                                style: const TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              addVerticalSpace(20),
                              ActionChip(
                                  avatar: const Icon(
                                    Icons.power_settings_new,
                                  ),
                                  label: const Text("تسجيل خروج"),
                                  onPressed: () {
                                    session.deleteUserInfo();

                                    Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                            pageBuilder: (_, __, ___) =>
                                                const GoogleLogin()));
                                  })
                            ],
                          ),
                        );
                      } else {
                        return const Text(" ");
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
