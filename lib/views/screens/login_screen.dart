import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/session.dart';
import 'package:loggin_google/views/screens/toon_tube/toon_tube_screen.dart';
import 'package:loggin_google/views/shared/shared_functions.dart';
import 'package:shimmer/shimmer.dart';

class GoogleLogin extends StatefulWidget {
  const GoogleLogin({Key? key}) : super(key: key);

  @override
  _GoogleLoginState createState() => _GoogleLoginState();
}

class _GoogleLoginState extends State<GoogleLogin> {
  bool _isLoggedInGoogle = false;
  bool _isLoggedInFacebook = false;
  Map _userObjFacebook = {};
  late GoogleSignInAccount _userObjGoogle;
  late GoogleSignIn _googleSignIn;
  DateTime timeBackPress=DateTime.now();

  Session? session;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    session = Session("", "", "");
    _googleSignIn = GoogleSignIn();
    _googleSignIn.signOut();
    FacebookAuth.instance.logOut();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final difference  =DateTime.now().difference(timeBackPress);
        final ifExitWarning=difference>=Duration(seconds: 2);
        timeBackPress=DateTime.now();
        if(ifExitWarning) {
          const message='press back again tow exit app';
          Fluttertoast.showToast(msg: message,fontSize: 18);
          return false;
        }else {
          Fluttertoast.cancel();
          SystemNavigator.pop();
          return true;
        }
      },
      child: SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  addVerticalSpace(150),
                  Shimmer.fromColors(
                    period: const Duration(milliseconds: 3000),
                    baseColor: Constant.primaryColor,
                    highlightColor: Colors.tealAccent,
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                      child: Text(
                        "ToonTube",
                        style: TextStyle(
                            fontSize: 50.0,
                            fontFamily: 'Pacifico',
                            shadows: <Shadow>[
                              Shadow(
                                  blurRadius: 18.0,
                                  color: Constant.primaryColor,
                                  offset: Offset.fromDirection(120, 12))
                            ]),
                      ),
                    ),
                  ),
                  addVerticalSpace(90),
                  Container(
                    child: _isLoggedInGoogle ? Column() : googleLoginButton,
                  ),
                  addVerticalSpace(10),
                  Container(
                      child:
                          _isLoggedInFacebook ? Column() : facebookLoginButton),
                ],
              ),
            )),
      ),
    );
  }

  Center get googleLoginButton {
    return Center(
        child: InkWell(
          onTap: () {
            _googleSignIn.signIn().then((userData) {
              setState(() {
                _isLoggedInGoogle = true;
                _userObjGoogle = userData!;

                session!.loginSuccess(true);
                session =
                    Session(_userObjGoogle.email, _userObjGoogle.photoUrl!,
                        _userObjGoogle.displayName!);
                session!.saveUserInfo(_userObjGoogle.email,
                    _userObjGoogle.photoUrl!, _userObjGoogle.displayName!);
                Navigator.push(
                    context,
                    PageRouteBuilder(
                        pageBuilder: (_, __, ___) => const MainHome()));
              });
            }).catchError((e) {
              // print(e);
            });
          },
          child: Image.asset("assets/images/google.png", width: 300,),
        )
    );
  }

  Center get facebookLoginButton {
    return Center(
        child: InkWell(
          onTap: () async {
            FacebookAuth.instance.login(permissions: [
              "public_profile",
              "email",
            ]).then((value) {
              FacebookAuth.instance.getUserData().then((userData) {
                setState(() {
                  _isLoggedInFacebook = true;
                  _userObjFacebook = userData;
                  session!.loginSuccess(true);
                  session = Session(
                      _userObjFacebook["email"],
                      _userObjFacebook["picture"]["data"]["url"],
                      _userObjFacebook["name"]);
                  session!.saveUserInfo(
                      _userObjFacebook["email"],
                      _userObjFacebook["picture"]["data"]["url"],
                      _userObjFacebook["name"]);
                  Navigator.push(
                      context,
                      PageRouteBuilder(
                          pageBuilder: (_, __, ___) => const MainHome()));
                });
              });
            });
          },
          child: Image.asset("assets/images/fb.png", width: 300,),
        )
    );
  }
}
