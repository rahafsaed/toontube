import 'dart:async';

import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/views/screens/login_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/toon_tube_screen.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future onCheckGoogleLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    if (preferences.getBool("sessionActive") != null) {
      bool success = preferences.getBool("sessionActive")!;
      if (success) {

        Navigator.push(
            context, PageRouteBuilder(pageBuilder: (_, __, ___) => const MainHome()));
      } else {
        Navigator.push(context,
            PageRouteBuilder(pageBuilder: (_, __, ___) => const GoogleLogin()));
      }
    } else {
      Navigator.push(context,
          PageRouteBuilder(pageBuilder: (_, __, ___) => const GoogleLogin()));
    }
  }


  startTime() async {
    var _duration = const Duration(seconds: 5);
    return Timer(_duration, onCheckGoogleLogin);
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body:
          Container(
            color: Constant.primaryColor,
          height: double.infinity,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Lottie.asset("assets/lottie/video_splash.json"),
              ),
            ],
          ),
        ),

      ),
    );
  }
}
