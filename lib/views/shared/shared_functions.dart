import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';

Color postInfoIconColor = Colors.blue;
  
Widget addVerticalSpace(double height){
  return SizedBox(
    height:height
  );
}

Widget addHorizontalSpace(double width){
  return SizedBox(
      width:width
  );
}

Widget buildContainer(Widget child, BuildContext context) {
    var dh = MediaQuery.of(context).size.height;
    return Container(
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(8.0),
      height: dh * 0.57,
      child: child,
    );
  }

Padding buildCategoryChips(String category) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Chip(
        label: Text(
          category,
          style: TextStyle(color: Constant.secondaryColor),
        ),
        backgroundColor: Constant.primaryColor,
      ),
    );
  }

Padding getRichText(IconData icon, String text) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: RichText(
        overflow: TextOverflow.ellipsis,
        text: TextSpan(
          children: [
            WidgetSpan(
                child: Icon(
              icon,
              color: postInfoIconColor,
              size: 12,
            )),
            TextSpan(
              text: text,
              style: const TextStyle(
                  color: Colors.grey,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
    );
  }
