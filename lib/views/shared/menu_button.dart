import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/database/db_helper.dart';
import 'package:loggin_google/views/screens/favorite_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/add_post_screen.dart';

class MenuButton extends StatelessWidget {
  const MenuButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
        icon: const Icon(Icons.more_vert),
        itemBuilder: (context) => <PopupMenuEntry>[
              PopupMenuItem(
                  child: ListTile(
                leading: Icon(
                  Icons.favorite_border,
                  color: Constant.primaryColor,
                ),
                title: const Text("My Favorite"),
                onTap: () async {
                  List<Map<String, dynamic>> favoriteList =
                      await DBHelper.getFavorites();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              FavoriteScreen(favorites: favoriteList)));
                },
              )),
              PopupMenuItem(
                  child: ListTile(
                leading: Icon(
                  Icons.edit,
                  color: Constant.primaryColor,
                ),
                title: const Text("Write Post"),
                onTap: () {
                  Navigator.push(
                      context,
                      PageRouteBuilder(
                          pageBuilder: (_, __, ___) => const AddPost()));
                },
              )),
            ]);
  }
}
