import 'package:flutter/material.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/session.dart';
import 'package:loggin_google/core/models/user_info.dart';
import 'package:loggin_google/views/screens/login_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/community/community_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/home/home_screen.dart';
import 'package:loggin_google/views/screens/toon_tube/watch/watch_screen.dart';

class AppDrawer extends StatelessWidget {

  Session session = Session("", "" ,"") ;

  AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(),
          _createDrawerItem(icon: Icons.home, text: 'الصفحة الرئيسية',
            onTap: () {
              Navigator.push(
                  context, PageRouteBuilder(pageBuilder: (_, __, ___) => const Home()));
            },),
          _createDrawerItem(icon: Icons.favorite, text: 'المفضلة',
            onTap: () {
              // Navigator.push(
              //     context, PageRouteBuilder(pageBuilder: (_, __, ___) => MainHome()));
            },),
          _createDrawerItem(icon: Icons.article, text: 'تواصل اجتماعي',
            onTap: () {
              Navigator.push(
                  context, PageRouteBuilder(pageBuilder: (_, __, ___) => const Community()));
            },),
          _createDrawerItem(icon: Icons.live_tv_outlined, text: 'المشاهدة',
              onTap: () {
                Navigator.push(
                    context, PageRouteBuilder(pageBuilder: (_, __, ___) => const Watch()));
              }),
          const Divider(),
          _createDrawerItem(
              icon: Icons.power_settings_new,
              text: 'تسجيل الخروج',
              onTap: () {
                session.deleteUserInfo();

                Navigator.push(
                    context,
                    PageRouteBuilder(
                        pageBuilder: (_, __, ___) => const GoogleLogin()));
              }),

        ],
      ),
    );
  }

  Widget _createHeader() {

    Future<UserInfo> userInfo  = session.loadUserInfo();


    return FutureBuilder<UserInfo>(
      future: userInfo,
      builder: ( context,  snapshot) {
        if (snapshot .hasData) {
          return   DrawerHeader(
            margin: EdgeInsets.zero,
            padding: EdgeInsets.zero,
            child: UserAccountsDrawerHeader(
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage('assets/images/login.jpeg'))),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(snapshot.data!.photoUrl!),
                radius: 50,
              ),
              accountName: Text(
                snapshot.data!.displayName!,
                style: TextStyle(
                    fontSize: 20,
                    color: Constant.primaryColor,
                    fontWeight: FontWeight.bold),
              ),
              accountEmail: Text(
                snapshot.data!.email!,
                style: TextStyle(
                    fontSize: 20,
                    color: Constant.primaryColor,
                    fontWeight: FontWeight.bold),
              ),
              //onDetailsPressed: (){},
            ),
          );
        } else {
          return Text("${snapshot.error}");
        }
      },
    );

  }

  Widget _createDrawerItem(
      {required IconData icon, required String text, required GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon, color: Constant.primaryColor,),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 20),
            child: Text(text, style: const TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold),),
          )
        ],
      ),
      onTap: onTap,
    );
  }
}
