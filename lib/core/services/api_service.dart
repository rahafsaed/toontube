import 'dart:convert';
import 'package:http/http.dart';
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/api/article.dart';

class ApiService {
  Future<List<Article>> getArticle() async {
    final res = await get(Uri.parse(Constant.baseUrlPost), headers: {
      'Content_Type': 'application/json',
    });

    if (res.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(res.body);

      List<dynamic> body = json['articles'];

      List<Article> article =
          body.map((dynamic item) => Article.fromJson(item)).toList();

      return article;
    } else {
      throw ("can't get the Article");
    }
  }

  Future<int> getResponse() async {
    final res = await get(Uri.parse(Constant.baseUrlPost), headers: {
      'Content_Type': 'application/json',
    });

    return res.statusCode;
  }
}
