import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:loggin_google/core/config/constants.dart';
import 'package:loggin_google/core/models/api/video.dart';

class ServerVideo {
  Future<List<Video>> getListVideo() async {
    var fullUrl = Constant.baseUrlVideo + "/getallgameshecoaches/";
    final response = await http.get(Uri.parse(fullUrl));
    final parsed = json.decode(response.body).cast<Map<String, dynamic>>();
    return parsed.map<Video>((json) => Video.fromJson(json)).toList();
  }
}
