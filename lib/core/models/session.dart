import 'dart:convert';

import 'package:loggin_google/core/models/user_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Session {
  String email;
  String displayName;
  String photoUrl;

  Session(this.email, this.photoUrl, this.displayName);

  Future loginSuccess(bool isLog) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool("sessionActive", isLog);
  }

  Future saveUserInfo(String email, String photo, String name) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    final user = UserInfo(
        email: this.email, displayName: displayName, photoUrl: photoUrl);
    String json = jsonEncode(user);
    preferences.setString("session_active", json);
  }

  Future<UserInfo> loadUserInfo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? json = preferences.getString("session_active");
    if (json == null) {
      throw Exception("Not Load");
    } else {
      Map<String, dynamic> map = jsonDecode(json);
      final userInfo = UserInfo.fromJson(map);
      return userInfo;
    }
  }

  Future deleteUserInfo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print("delete info");
    preferences.clear();
    print("delete info ${preferences.getBool("sessionActive")}");
  }
}
