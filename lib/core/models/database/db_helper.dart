import 'package:sqflite/sqflite.dart';

import '../api/article.dart';

class DBHelper {
  static Database? _db;
  static const int _version = 1;
  static const String _tableName = "posts";

  static Future<void> initDB() async {
    if (_db != null) {
      return;
    } else {
        String _path = await getDatabasesPath() + 'post.db';
        _db = await openDatabase(_path, version: _version,
            onCreate: (Database db, int version) async {
          // When creating the db, create the table
          await db.execute('CREATE TABLE $_tableName ('
              'id STRING, '
              'author STRING, title STRING PRIMARY KEY, description STRING, '
              ' url STRING, urlToImage STRING, publishedAt STRING, '
              'content STRING)');
        });
    }
  }

  static Future<int> insert(Article? article) async {
    return await _db!.insert(_tableName, article!.toJson());
  }

  static Future<bool> checkFavorite(String? title) async {
    List<Map<String, Object?>> list = await _db!.rawQuery(
        "SELECT * FROM $_tableName WHERE title = ?", [title!]);
    if (list.isEmpty) {
      return false;
    }
    return true;
  }

  static Future<List<Map<String, dynamic>>> getFavorites() async {
    return await _db!.query(_tableName);
  }

  static Future<int> delete(Article? article) async {
    return await _db!
        .delete(_tableName, where: 'id = ?', whereArgs: [article!.source.id]);
  }


}
