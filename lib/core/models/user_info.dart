class UserInfo {
  String? email;
  String? displayName;
  String? photoUrl;

  UserInfo(
      {required this.email, required this.displayName, required this.photoUrl});

  Map<String, dynamic> toJson() {
    return {"email": email, "displayName": displayName, "photoUrl": photoUrl};
  }

  UserInfo.fromJson(Map<String, dynamic> json) {
    email = json["email"];
    displayName = json["displayName"];
    photoUrl = json["photoUrl"];
  }
}
