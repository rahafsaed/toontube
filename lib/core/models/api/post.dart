import 'package:flutter/material.dart';


class Post {
  int? id;
  String? title;
  String? authorName;
  String? date;
  double? rate;
  String? imgURL;
  String? details;
  int? isFavorite;


  int? get getId => id;

  String? get getTitle => title;

  String? get getAuthorName => authorName;

  String? get getDate => date;

  double? get getRate => rate;

  String? get getDetails => details;

  String? get getImgURL => imgURL;

  int? get getIsFavorite => isFavorite;

Post(
{
  @required this.id,
  @required this.title,
  @required this.authorName,
  @required this.date,
  @required this.rate,
  @required this.imgURL,
  @required this.details,
  @required this.isFavorite});

}
