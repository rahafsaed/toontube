// ignore_for_file: prefer_typing_uninitialized_variables

class Video {
  var id, title, url, introStart, introEnd, outroStart, outroEnd, seriesId;

  Video({
    this.id = 1,
    this.title = "loading..",
    this.introStart = "loading..",
    this.introEnd = "loading..",
    this.outroStart = 0,
    this.outroEnd = "loading..",
    this.seriesId = "loading..",
    this.url = "nnnnbnb",
  });

  factory Video.fromJson(Map<String, dynamic> json) {
    return Video(
      id: json['id'],
      title: json['title'],
      url: json['url'],
      introStart: json['intro_start'],
      introEnd: json['intro_end'],
      outroStart: json['outro_start'],
      outroEnd: json['outro_end'],
      seriesId: json['series_id'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['url'] = url;
    data['intro_start'] = introStart;
    data['intro_end'] = introEnd;
    data['outro_start'] = outroStart;
    data['outro_end'] = outroEnd;
    data['series_id'] = seriesId;
    return data;
  }
}
