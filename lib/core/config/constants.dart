import 'package:flutter/material.dart';

class Constant {
  static String baseUrlVideo = "http://10.0.3.2:8000/api/coach";
  static Text titleAppBar = const Text(
    "ToonTube",
    style: TextStyle(fontWeight: FontWeight.bold),
  );
  static String baseUrlPost =
      "https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=c2b0651c3f3e41de9223b84e0e9f26d6";
  static Color primaryColor = Colors.teal;
  static Color secondaryColor = Colors.white;
}
