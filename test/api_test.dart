import 'package:flutter_test/flutter_test.dart';
import 'package:loggin_google/core/services/api_service.dart';


void main() {
  test("api test", () async {
    ApiService api = ApiService();
    var response = await api.getResponse();
    expect(200, response);
  });
}
